import { createContext, useEffect, useState } from "react";
import "./App.css";
import Grid from "@mui/material/Grid";
import ProductCard from "./components/productCard";
import { db } from "./firebase";
import { collection, getDocs } from "firebase/firestore";
import { Product } from "./types";

export const UserContext = createContext(null);


function App() {
  //get a collection called 'products' using useEffect in firebase firestore


  const [products, setProducts] = useState<Product[]>([]);
  const [cart, setCart] = useState<Product[]>([]);

  const fetchProducts = async () => {
    console.log("fetching products");

    await getDocs(collection(db, "products"))
        .then((querySnapshot)=>{
          console.log("querySnapshot", querySnapshot);
          setProducts([]);
            querySnapshot.forEach((doc)=>{
              console.log("doc", doc.data());
                const product = doc.data() as Product;
                product.id = doc.id;
                setProducts((products) => [...products, product]);
            })
        })

        let cartData = localStorage.getItem('cart');
        if(cartData) {
          cartData = JSON.parse(cartData);
          //convert to product type
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          cartData = cartData.map((product: any) => {
            product.price = parseFloat(product.price);
            product.stock = parseInt(product.stock);
            product.id = product.id.toString();
            product.image = product.image.toString();
            product.name = product.name.toString();
            return product;
          });
          setCart(cartData);
        }

}

  useEffect(() => {
    fetchProducts();
  }, []);

  return (
    <>
      <Grid
        container
        spacing={{ xs: 2, md: 3 }}
        columns={{ xs: 1, sm: 8, md: 12 }}
        p={2}
      >
        {Array.from(products).map((product, index) => (
          <Grid item xs={2} sm={4} md={4} key={index}>
            <ProductCard product={product} setCart={setCart} cart={cart}/>
          </Grid>
        ))}
      </Grid>
    </>
  );
}

export default App;

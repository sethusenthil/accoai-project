//create type called Product
type Product = {
    name: string;
    price: number;
    description: string;
    stock: number;
    image: string;
    id: string;
};

export type { Product };
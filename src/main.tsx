import React, { useState } from "react";
import ReactDOM from "react-dom/client";
import App from "./App.tsx";
import "./index.css";
import { createBrowserRouter, RouterProvider } from "react-router-dom";

import { ThemeProvider, createTheme } from "@mui/material";
import NavBar from "./components/navbar.tsx";
import ProductViewer from "./pages/productViewer.tsx";
import ViewCart from "./pages/viewCart.tsx";

const darkTheme = createTheme({
  palette: {
    mode: "dark",
  },
});

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
  },
  {
    path: "/cart",
    element: <ViewCart />,
  },
  {
    path: "/product/:productID",
    element: <ProductViewer />,
  },
]);

const Main = () => {
  return (
    <React.StrictMode>
      <ThemeProvider theme={darkTheme}>
        <NavBar />
        <RouterProvider router={router} />
      </ThemeProvider>
    </React.StrictMode>
  );
};

ReactDOM.createRoot(document.getElementById("root")!).render(<Main />);

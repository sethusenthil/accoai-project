import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { Button, CardActionArea, CardActions, Chip } from "@mui/material";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import { Product } from "../types";

export default function ProductCard(props: {
  product: Product;
  setCart: React.Dispatch<React.SetStateAction<Product[]>>;
  cart: Product[];
}) {
  return (
    <Card sx={{ maxWidth: 345 }}>
      <CardActionArea
        onClick={() => {
          console.log("props.product.id", props.product.id);
          window.location.href = `/product/${props.product.id}`;
        }}
      >
        <CardMedia
          component="img"
          height="140"
          image={props.product.image}
          alt="green iguana"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {props.product.name}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {props.product.description}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions sx={{ justifyContent: "space-around" }}>
        <Button
          variant="contained"
          startIcon={<AddShoppingCartIcon />}
          onClick={() => {
            localStorage.setItem(
              "cart",
              JSON.stringify([...props.cart, props.product])
            );
            props.setCart((cart) => [...cart, props.product]);
          }}
        >
          Add to Cart
        </Button>

        <Chip label={props.product.stock + " left"} />

        <Chip label={"$" + props.product.price} />
      </CardActions>
    </Card>
  );
}


// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyDTpoYTfU6uJnBhlXH_jE2so_yXLgv5PsI",
    authDomain: "backup-codes-backify.firebaseapp.com",
    databaseURL: "https://backup-codes-backify-default-rtdb.firebaseio.com",
    projectId: "backup-codes-backify",
    storageBucket: "backup-codes-backify.appspot.com",
    messagingSenderId: "1089849615393",
    appId: "1:1089849615393:web:de7ea6351825a3157e6f60",
    measurementId: "G-L0ZFLVQ40Y"
  };
// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
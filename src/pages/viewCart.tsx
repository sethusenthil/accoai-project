import React, { useEffect, useState } from "react";
import {
  Typography,
  Card,
  CardContent,
  CardMedia,
  CardActions,
  Button,
  Chip,
} from "@mui/material";
import { useParams } from "react-router-dom";
import { Product } from "../types";
import { collection, doc, getDoc, updateDoc } from "firebase/firestore";
import { db } from "../firebase";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";

function ViewCart() {
  const [cart, setCart] = useState<Product[]>([]);

  const fetchProduct = async () => {
    let cartData = localStorage.getItem("cart");
    if (cartData) {
      cartData = JSON.parse(cartData);
      //convert to product type
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      cartData = cartData.map((product: any) => {
        product.price = parseFloat(product.price);
        product.stock = parseInt(product.stock);
        product.id = product.id.toString();
        product.image = product.image.toString();
        product.name = product.name.toString();
        return product;
      });
      setCart(cartData);
    }
  };

  useEffect(() => {
    fetchProduct();
  }, []);

  return (
    <Card>
      <CardContent>
        <Typography variant="h5" component="div">
          You have {cart.length} items in your cart
        </Typography>
        {cart.map((product) => (
          <div key={product.id}>
            <CardMedia
              component="img"
              height="140"
              image={product.image}
              alt={product.name}
            />
            <CardContent>
              <Typography variant="h6" component="div">
                {product.name}
              </Typography>
              <Typography variant="body2" color="text.secondary">
                Price: ${product.price}
              </Typography>
              <Typography variant="body2" color="text.secondary">
                Stock: {product.stock}
              </Typography>
            </CardContent>
          </div>
        ))}
        <Typography variant="body1" color="text.secondary">
          Total Price: ${cart.reduce((acc, curr) => acc + curr.price, 0)}
        </Typography>
        <Typography variant="body1" color="text.secondary"></Typography>
        <Typography variant="h6" component="div"></Typography>
        <Typography variant="h6" component="div"></Typography>
      </CardContent>
      <CardActions sx={{ justifyContent: "space-around" }}>
        <Button
          disabled={cart.length > 0 ? false : true}
          variant="contained"
          startIcon={<AddShoppingCartIcon />}
          onClick={async () => {
            var err = false;
            //reduce the stock of the products
            const reduceStock = async () => {
              // Loop through the products in the cart
              for (const product of cart) {
                // Get the document reference for the product in the firestore database
                const productRef = doc(db, "products", product.id);

                // Get the product document from the firestore database
                const productDoc = await getDoc(productRef);

                // Check if the product document exists
                if (productDoc.exists()) {
                  // Get the current stock value from the product document
                  const currentStock = productDoc.data().stock;

                  // Calculate the new stock value after reducing the stock
                  const newStock = currentStock - 1;
                  // Check if the new stock value is less than 0
                  if (newStock < 0) {
                    alert(`Not enough stock for ${product.name}`);
                    err = true;
                    return;
                  }

                  // Update the stock value in the firestore database
                  await updateDoc(productRef, { stock: newStock });
                }
              }
            };

            await reduceStock();
            if (err) return;
            // Clear the cart after placing the order
            localStorage.removeItem("cart");
            setCart([]);
            alert("Order placed successfully!");
            //go to home page
            window.location.href = "/";
          }}
        >
          Place Order
        </Button>
      </CardActions>
    </Card>
  );
}

export default ViewCart;

import React, { useEffect, useState } from "react";
import {
  Typography,
  Card,
  CardContent,
  CardMedia,
  CardActions,
  Button,
  Chip,
} from "@mui/material";
import { useParams } from "react-router-dom";
import { Product } from "../types";
import { collection, doc, getDoc } from "firebase/firestore";
import { db } from "../firebase";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";

function ProductViewer() {
  const { productID } = useParams();
  console.log("productID", productID);

  const [product, setProduct] = useState<Product>();

  const fetchProduct = async () => {
    console.log("fetching product");

    const docRef = doc(collection(db, "products"), productID);
    const docSnap = await getDoc(docRef);
    if (docSnap.exists()) {
      const product = docSnap.data() as Product;
      console.log("product", product);
      setProduct(product);
    } else {
      console.log("Product not found");
    }
  };

  useEffect(() => {
    fetchProduct();
  }, []);

  return (
    <Card>
      <CardMedia
        sx={{ height: 500 }}
        image={product?.image}
        title="green iguana"
      />
      <CardContent>
        <Typography variant="h5" component="div">
          {product?.name}
        </Typography>
        <Typography variant="body1" color="text.secondary">
          {product?.description}
        </Typography>
        <Typography variant="h6" component="div">
          Price: ${product?.price}
        </Typography>
        <Typography variant="h6" component="div">
          Only {product?.stock} left! Buy Fast
        </Typography>
      </CardContent>
      <CardActions sx={{ justifyContent: "space-around" }}>
        <Button variant="contained" startIcon={<AddShoppingCartIcon />}>
          Add to Cart
        </Button>
      </CardActions>
    </Card>
  );
}

export default ProductViewer;
